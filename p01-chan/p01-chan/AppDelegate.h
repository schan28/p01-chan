//
//  AppDelegate.h
//  p01-chan
//
//  Created by Stanley Chan on 1/31/16.
//  Copyright (c) 2016 Binghamton University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

