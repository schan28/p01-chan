//
//  ViewController.h
//  p01-chan
//
//  Created by Stanley Chan on 1/31/16.
//  Copyright (c) 2016 Binghamton University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *textLabel;

@property (strong, nonatomic) IBOutlet UIButton *textSwitchButton;

@property (strong, nonatomic) IBOutlet UIButton *redText;

@property (strong, nonatomic) IBOutlet UIButton *cyanText;

@property (strong, nonatomic) IBOutlet UIImageView *thumbsUpImage;

@end

