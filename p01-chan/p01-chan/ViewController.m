//
//  ViewController.m
//  p01-chan
//
//  Created by Stanley Chan on 1/31/16.
//  Copyright (c) 2016 Binghamton University. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize textLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor colorWithRed:(255/255.0) green: (239/255.0) blue:(213/255.0) alpha:1];
    textLabel.numberOfLines = 2;
    [_redText setHidden: TRUE];
    [_cyanText setHidden: TRUE];
        _thumbsUpImage.image = [UIImage imageNamed:@"thumbsUpBefore"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)textSwitchButton:(id)sender {
    [textLabel setText:@"Stanley Chan completed the first assignment."];
    [_textSwitchButton setHidden: TRUE];
    [_redText setHidden: FALSE];
    [_cyanText setHidden: FALSE];
    
    _thumbsUpImage.image = [UIImage imageNamed:@"thumbsUp"];

}


- (IBAction)redText:(id)sender {
    UIColor *newTextColor = [UIColor redColor];
    [textLabel setTextColor: newTextColor];
}

- (IBAction)cyanText:(id)sender {
    UIColor *newTextColor = [UIColor cyanColor];
    [textLabel setTextColor: newTextColor];
}



@end
